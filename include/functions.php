<?php

date_default_timezone_set('Europe/Oslo');
setlocale(LC_TIME, 'no_NO');

function getLineBreak() {
    return "\n";
}

function getLine($size=48) {
    $line = '';
    for ($i=0;$i<$size;$i++) {
        $line .= '-';
    }
    return $line;
}

function _dump($var) {
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}

function get_http_codes() {
    return array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended',
        601 => 'Print server not found',
        602 => 'Custom Status',
        603 => 'Custom Status',
        604 => 'Custom Status',
        605 => 'Custom Status',
    );
}

function returnJson($httpStatus, $items=array(), $extra=array()) {
	$http_codes = get_http_codes();
	$return = array(
		'status' => array(
			'code' => $httpStatus,
			'status' => @$http_codes[$httpStatus]
		),
		'items' => $items,
		'extra' => $extra
	);
	die(json_encode($return, JSON_PRETTY_PRINT));
}

function formatMoney($num) {
    if (!is_numeric($num)) return $num;
    return '' . number_format($num, 2, '.', '');
}

class Item {
    private $name;
    private $qty;
    private $price;
    private $total_price;

    public function __construct($name = '', $qty = '', $price = '', $total_price = false) {
        $this->name = $name;
        $this->qty = $qty;
        $this->price = $price;
        $this->total_price = $total_price;
    }

    private function formatMoney($num) {
        if (!is_numeric($num)) return $num;
        return '' . number_format($num, 2, '.', '');
    }

    public function getAsString($width = 48) {
        $cols_price       = 16;
        $cols_total_price = 10;
        $cols_name        = $width - ($cols_price + $cols_total_price);

        if ($this->total_price == false) {
            $this->total_price = $this->qty * $this->price;
            $this->price = $this->qty .'X'. $this->formatMoney($this->price);
        }

        $this->total_price =  $this->formatMoney($this->total_price);

        // trata nomes grandes
        $name_after = '';
        $length_limit = 20;
        $name_length = strlen($this->name);
        if ($name_length > $length_limit) {
            $times = ceil($name_length / $length_limit);
            for ($i=1;$i<$times;$i++) {
                $name_after .= '  ' . substr($this->name, ($i*$length_limit), $length_limit) . "\n";
            }
            $this->name = substr($this->name, 0, $length_limit);
        }

        $name = str_pad($this->name, $cols_name, ' ');
        $price = str_pad($this->price, $cols_price, ' ', STR_PAD_LEFT);
        $total_price = str_pad($this->total_price, $cols_total_price, ' ', STR_PAD_LEFT);

        $return = "$name$price$total_price\n" . $name_after;

        return $return;
    }

    public function __toString() {
        return $this->getAsString();
    }

}
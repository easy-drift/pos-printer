<?php
header('Content-type: application/json');
include ('include/functions.php');
include ('escpos/autoload.php');

$data = json_decode(file_get_contents('php://input'), true);

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\ImagickEscposImage;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

if (!$data['printer']) {
    returnJson(404);
}

// select printer
if (strtolower($data['printer']['type']) == 'usb') {
    $connector = new WindowsPrintConnector($data['printer']['name']);
}
else if (strtolower($data['printer']['type']) == 'ethernet') {
    $connector = new NetworkPrintConnector($data['printer']['name'], 9100);
}

$printer = new Printer($connector);

try {
    $order = $data['order'];
    $company = $data['company_data'];

    $subtotal = new Item('Subtotal', '', '', $order['amount'] - $order['vat']);
    $tax = new Item('MVA', '', '', $order['vat']);
    $total = new Item('Total', '', '', $order['amount']);
    /* Date is kept the same for testing */
	//$date = date('l jS \of F Y h:i:s A');
    $date = date('d M Y H:i');

    /* Start the printer */
    $printer = new Printer($connector);

    /* Header */
    $printer->setJustification(Printer::JUSTIFY_CENTER);
    $printer->setTextSize(2, 2);
    $printer->text($company['name'] . getLineBreak());
    $printer->selectPrintMode();
    $printer->text($company['address'] . getLineBreak());
    $printer->text($company['city'] . ' - ' . $company['area'] . getLineBreak());
    $printer->text('NO ' . $company['phone'] . getLineBreak());
    $printer->text(getLine());
    $printer->feed();

    /* Title of receipt */
    $printer->setEmphasis(true);
    $printer->text("Salskvittering\n");
    $printer->text(getLine());
    $printer->feed();

    /* Order information */
    $printer->setEmphasis(false);
    $printer->text('OID: ' . $order['order_identify'] . getLineBreak());
    $printer->text('TID: ' . $order['pos_identify'] . getLineBreak());
    $printer->text('Dato: ' . $date . getLineBreak());
    $printer->text(getLine());
    $printer->feed();

    /* Items */
    $printer->setJustification(Printer::JUSTIFY_LEFT);
    $printer->setEmphasis(true);
    $printer->text(new Item('ITEM', 'QTY', 'PRICE', 'TOTAL'));
    $printer->setEmphasis(false);
    foreach ($data['order_lines'] as $line) {
    	$item = new Item($line['name'], $line['qty'], $line['price']);
        $printer->text($item->getAsString()); // for 58mm Font A
    }

    /* Order Total */
    $printer->setEmphasis(true);
    $printer->text($total->getAsString());
    $printer->setEmphasis(false);
    $printer->feed();

    /* Order Vat */
    if ($data['vat_lines']) {
        foreach ($data['vat_lines'] as $key => $value) {
            $net = (double)$value['amount'] - (double)$value['vat'];
            $line_vat = new Item(formatMoney($value['vat']) . ' ' .$value['name'], '', '', $value['amount']);
            $line_net = new Item('Net TTL', '', '', $net);
            $printer->text($line_vat->getAsString());
            $printer->text($line_net->getAsString());
        }
        $printer->text(getLine());
        $printer->feed();
    }

    /* Aditional info */
    if (isset($data['pos_settings']['message_cupom'])) {
        $printer->text($data['pos_settings']['message_cupom']);
        $printer->feed();
    }

    /* Cut the receipt and open the cash drawer */
    $printer->cut();
    $printer->pulse();

} catch (Exception $e) {
    echo $e->getMessage();
} finally {
    $printer->close();
}

returnJson(200, $data);
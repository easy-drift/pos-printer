<?php
header('Content-type: application/json');
include ('include/functions.php');
include ('escpos/autoload.php');

$data = json_decode(file_get_contents('php://input'), true);

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\ImagickEscposImage;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

if (!$data['printer']) {
    returnJson(404);
}

if (strtolower($data['printer']['type']) == 'usb') {
    $connector = new WindowsPrintConnector($data['printer']['name']);
}
else if (strtolower($data['printer']['type']) == 'ethernet') {
    $connector = new NetworkPrintConnector($data['printer']['name'], 9100);
}

$printer = new Printer($connector);

$logo = EscposImage::load("resources/easy-step-logo.png", false);

try {
	/* Print top logo */
	// $printer -> setJustification(Printer::JUSTIFY_CENTER);
	// $printer -> graphics($logo);
	// $printer->pulse();

    $subtotal = new item('Subtotal', '', '', $data['total']-$data['tax']);
    $tax = new item('MVA', '', '', $data['tax']);
    $total = new item('Total', '', '', $data['total']);
    /* Date is kept the same for testing */
	$date = date('l jS \of F Y h:i:s A');

    /* Start the printer */
    $printer = new Printer($connector);

    /* Name of shop */
    $printer->setJustification(Printer::JUSTIFY_CENTER);
    $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $printer->text("Easy Step AS.\n");
    $printer->selectPrintMode();
    $printer->text("Shop No. 42.\n");
    $printer->feed();


    /* Title of receipt */
    $printer->setEmphasis(true);
    $printer->text("SALES INVOICE\n");
    $printer->setEmphasis(false);

    /* Items */
    $printer->setJustification(Printer::JUSTIFY_LEFT);
    $printer->setEmphasis(true);
    $printer->text(new item('ITEM', 'QTY', 'PRICE', 'TOTAL'));
    $printer->setEmphasis(false);
    foreach ($data['order_lines'] as $line) {
    	$item = new item($line['name'], $line['qty'], $line['price']);
        $printer->text($item->getAsString()); // for 58mm Font A
    }
    //die();
    $printer->setEmphasis(true);
    $printer->text($subtotal->getAsString());
    $printer->setEmphasis(false);
    $printer->feed();

    /* Tax and total */
    $printer->text($tax->getAsString());
    $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $printer->text($total->getAsString());
    $printer->selectPrintMode();

    /* Footer */
//     $printer->feed(2);
//     $printer->setJustification(Printer::JUSTIFY_CENTER);
//     $printer->text("Thank you for shopping\n");
//     $printer->text("at ExampleMart\n");
//     $printer->text("For trading hours,\n");
//     $printer->text("please visit example.com\n");
//     $printer->feed(2);
//     $printer->text($date . "\n");

//     /* Barcode Default look */

//     $printer->barcode("ABC", Printer::BARCODE_CODE39);
//     $printer->feed();
//     $printer->feed();


// // Demo that alignment QRcode is the same as text
//     $printer2 = new Printer($connector); // dirty printer profile hack !!
//     $printer2->setJustification(Printer::JUSTIFY_CENTER);
//     $printer2->qrCode("https://rawbt.ru/mike42", Printer::QR_ECLEVEL_M, 8);
//     $printer2->text("rawbt.ru/mike42\n");
//     $printer2->setJustification();
//     $printer2->feed();


    /* Cut the receipt and open the cash drawer */
    $printer->cut();
    $printer->pulse();

} catch (Exception $e) {
    echo $e->getMessage();
} finally {
    $printer->close();
}

/* A wrapper to do organise item names & prices into columns */

class item {
    private $name;
    private $qty;
    private $price;
    private $total_price;

    public function __construct($name = '', $qty = '', $price = '', $total_price = false) {
        $this->name = $name;
        $this->qty = $qty;
        $this->price = $price;
        $this->total_price = $total_price;
    }

    private function formatMoney($num) {
    	if (!is_numeric($num)) return $num;
    	return '' . number_format($num, 0, '', '.');
    }

    public function getAsString($width = 48) {
    	$cols_price       = 16;
    	$cols_total_price = 10;
    	$cols_name        = $width - ($cols_price + $cols_total_price);

    	if ($this->total_price == false) {
    		$this->total_price = $this->qty * $this->price;
    		$this->price = $this->qty .'X'. $this->formatMoney($this->price);
    	}

    	// trata nomes grandes
    	$name_after = '';
    	$length_limit = 20;
    	$name_length = strlen($this->name);
    	if ($name_length > $length_limit) {
    		$times = ceil($name_length / $length_limit);
    		for ($i=1;$i<$times;$i++) {
    			$name_after .= substr($this->name, ($i*$length_limit), $length_limit) . "\n";
    		}
    		$this->name = substr($this->name, 0, $length_limit);
    	}

        $name = str_pad($this->name, $cols_name, ' ');
        $price = str_pad($this->price, $cols_price, ' ', STR_PAD_LEFT);
        $total_price = str_pad($this->total_price, $cols_total_price, ' ', STR_PAD_LEFT);

        $return = "$name$price$total_price\n" . $name_after;

        return $return;
    }

    public function __toString() {
        return $this->getAsString();
    }

}

returnJson($data);